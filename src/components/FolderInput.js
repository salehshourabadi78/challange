import { useDispatch } from "react-redux";
import { newFolder, renameItem } from "../redux/modules/directories";
import { useState } from "react";
import { useParams } from "react-router-dom";

const NewFolder = ({ value, edit, folderIndex }) => {
  const dispatch = useDispatch();
  const [folderName, setFolderName] = useState(value);
  const { id } = useParams();
  return (
    <li className="folder-icon">
      <input
        type="text"
        name="folder-name"
        value={folderName}
        onChange={(e) => setFolderName(e.target.value)}
        onBlur={() => {
          if (edit) {
            dispatch(renameItem(folderName, folderIndex));
          } else {
            dispatch(newFolder(folderName, id));
          }
        }}
      />
    </li>
  );
};

export default NewFolder;

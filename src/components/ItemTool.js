import { useDispatch } from "react-redux";
import { setItemToInput, deleteItem } from "../redux/modules/directories";
import editIcon from "../assets/icons/edit.svg";
import deleteIcon from "../assets/icons/delete.svg";

const ItemTool = ({ id, index }) => {
  const dispatch = useDispatch();
  return (
    <div className="icons">
      <img src={editIcon} className="icon" onClick={() => dispatch(setItemToInput(id))} />
      <img src={deleteIcon} className="icon" onClick={() => dispatch(deleteItem(index))} />
    </div>
  );
};

export default ItemTool;

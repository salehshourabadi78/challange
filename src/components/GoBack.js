import { Link } from "react-router-dom";
const GoBack = ({ to }) => (
  <li>
    <Link to={`/${to}`}>...</Link>
  </li>
);

export default GoBack;

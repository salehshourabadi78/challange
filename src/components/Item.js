import { Link } from "react-router-dom";
import constants from "../util/const";
import styled from "styled-components";
import FolderInput from "./FolderInput";
import { useSelector } from "react-redux";
import ItemTool from "./ItemTool";

const Item = ({ type, name, id, size, index }) => {
  const { editFolder } = useSelector((state) => state.directoriesReducer);
  if (type === constants.itemType.FOLDER) {
    return (
      <>
        {editFolder && editFolder === id ? (
          <FolderInput value={name} folderIndex={index} edit={true} />
        ) : (
          <StyledItem className="folder-icon">
            <Link to={`/${id}`}>{name} </Link>
            <ItemTool id={id} index={index} />
          </StyledItem>
        )}
      </>
    );
  } else if (type === constants.itemType.FILE) {
    return (
      <>
        {editFolder && editFolder === id ? (
          <FolderInput value={name} folderIndex={index} edit={true} />
        ) : (
          <StyledItem className="file-icon">
            <p className="file-text">
              <span className="file-name">{name}</span> <br />{" "}
              <span className="file-size">{size}</span>
            </p>
            <ItemTool id={id} index={index} />
          </StyledItem>
        )}
      </>
    );
  }
};

const StyledItem = styled.li`
  .icon {
    height: 18px;
    cursor: pointer;
  }
`;

export default Item;

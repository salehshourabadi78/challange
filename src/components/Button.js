import styled from "styled-components";
const Button = (props) => (
  <StyledButton {...props}>
    {props.icon && <img src={props.icon} alt={props.children} />}
    <span>{props.children}</span>
  </StyledButton>
);

const StyledButton = styled.button`
  border: none;
  background: transparent;
  display: flex;
  align-items: center;
  cursor: pointer;
  border: 1px solid #eee;
  padding: 5px;
  img {
    height: 20px;
    margin-right: 5px;
  }
`;

export default Button;

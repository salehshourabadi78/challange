import Item from "./Item";
import constants from "../util/const";
import GoBack from "./GoBack";
import { findParent } from "../util";
import { useSelector } from "react-redux";
import NewFolder from "./FolderInput";
import styled from "styled-components";
const RenderItems = ({ folderID }) => {
  const { items, newFolderStatus } = useSelector((state) => state.directoriesReducer);

  let flag = false;
  let list = items.map((item, index) => {
    if (item.parent === folderID) {
      flag = true;
      return (
        <Item
          index={index}
          key={item.id}
          type={item.type}
          name={item.name}
          id={item.id}
          size={item.size}
        />
      );
    } else {
      return null;
    }
  });

  let res = null;

  if (flag) res = list;
  else res = <p className="not-found">not found</p>;

  return (
    <>
      <StyledList>
        {folderID !== constants.ROOT && <GoBack to={findParent(items, folderID)} />}
        {newFolderStatus && <NewFolder />}
        {res}
      </StyledList>
    </>
  );
};

const StyledList = styled.ul`
  display: flex;
  flex-direction: column;
  li {
    width: 30%;
    display: flex;
    justify-content: space-between;
  }
`;

export default RenderItems;

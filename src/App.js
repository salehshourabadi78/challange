import { BrowserRouter as Router, Switch, Route, Redirect } from "react-router-dom";
import Directories from "./pages/Directories";
function App() {
  return (
    <Router>
      <Switch>
        <Route exact path="/" render={() => <Redirect to="/root" />} />
        <Route path="/:id">
          <Directories />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;

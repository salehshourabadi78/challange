import { useParams } from "react-router-dom";
import RenderItems from "../components/RenderItems";
import ToolsBox from "../components/ToolsBox";

// render files and folders
const Directories = () => {
  let { id } = useParams();
  return (
    <>
      <div className="container">
        {/* render tools box */}
        <ToolsBox />
        <RenderItems folderID={id} />
      </div>
    </>
  );
};

export default Directories;

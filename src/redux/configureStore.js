import { combineReducers, createStore } from "redux";
import directoriesReducer from "./modules/directories";

const reducers = combineReducers({
  directoriesReducer,
});

const store = createStore(reducers);

export default store;

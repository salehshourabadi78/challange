import constant from "../../util/const";
import { v4 as uuidV4 } from "uuid";

// ACTIONS
const NEW_FOLDER = "directory/newFolder";
const NEW_FOLDER_STATUS_TOGGLE = "directory/newFolderStatusToggle";
const RENAME_ITEM = "directory/renameItem";
const SET_ITEM_INPUT = "directory/setItemInput";
const DELETE_ITEM = "directory/deleteItem";

const initialState = {
  items: [
    { id: "1", name: "songs", type: "folder", parent: "root", size: null },
    { id: "2", name: "pop", type: "folder", parent: "1", size: null },
    { id: "3", name: "Blank Space", type: "file", parent: "2", size: "2MB" },
    { id: "4", name: "video", type: "folder", parent: "root", size: null },
    { id: "5", name: "code", type: "folder", parent: "root", size: null },
    { id: "6", name: "learn", type: "folder", parent: "root", size: null },
  ],
  newFolderStatus: false,
  editFolder: null,
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case NEW_FOLDER:
      if (!action.payload.folderName) {
        return { ...state, newFolderStatus: false };
      }
      let newItems = [...state.items];
      newItems.push({
        id: uuidV4(),
        name: action.payload.folderName,
        parent: action.payload.parent,
        type: constant.itemType.FOLDER,
        size: null,
      });
      return { ...state, items: newItems, newFolderStatus: false };

    case RENAME_ITEM:
      let items = [...state.items];
      items[action.payload.folderIndex].name = action.payload.folderName;
      return { ...state, items, editFolder: null };

    case NEW_FOLDER_STATUS_TOGGLE:
      return { ...state, newFolderStatus: !state.newFolderStatus };
    case SET_ITEM_INPUT:
      return { ...state, editFolder: action.payload.id };
    case DELETE_ITEM:
      let removeItem = [...state.items];
      removeItem.splice(action.payload.itemIndex, 1);
      return { ...state, items: removeItem };
    default:
      return state;
  }
}

export const newFolder = (name, parent) => {
  return {
    type: NEW_FOLDER,
    payload: {
      folderName: name,
      parent: parent,
    },
  };
};

export const newFolderStatusToggle = () => {
  return {
    type: NEW_FOLDER_STATUS_TOGGLE,
  };
};

export const renameItem = (name, index) => {
  return {
    type: RENAME_ITEM,
    payload: {
      folderName: name,
      folderIndex: index,
    },
  };
};

export const setItemToInput = (id) => {
  return {
    type: SET_ITEM_INPUT,
    payload: {
      id: id,
    },
  };
};

export const deleteItem = (index) => {
  return {
    type: DELETE_ITEM,
    payload: {
      itemIndex: index,
    },
  };
};

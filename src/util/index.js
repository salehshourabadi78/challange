import constants from "./const";

export const findParent = (items, id) => {
  const found = items.find((element) => element.id === id);

  if (found) {
    return found.parent;
  } else {
    return constants.ROOT;
  }
};

export default {
  ROOT: "root",
  itemType: {
    FOLDER: "folder",
    FILE: "file",
    GO_BACK: "goBack",
  },
};
